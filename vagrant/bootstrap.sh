# Register trusted Microsoft signature key
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /etc/apt/trusted.gpg.d/microsoft.gpg

# Register the Microsoft Product apt repository
echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-xenial-prod xenial main" > /etc/apt/sources.list.d/dotnetdev.list

# Update apt sources #
apt-get update

# Upgrade APT #
apt-get upgrade -y

# Install Docker-Compose #
curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Install DotNet #
apt-get install -y dotnet-sdk-2.0.0

# Install PHP 7.1 and its most common modules #
apt-get install -y software-properties-common
add-apt-repository -y ppa:ondrej/php
apt-get update
apt-get upgrade -y
apt-get install -y php7.1 php7.1-cli php7.1-common php7.1-json php7.1-opcache php7.1-mysql php7.1-mbstring php7.1-mcrypt php7.1-zip php7.1-fpm php7.1-xml

# Install composer
apt-get install -y composer

# Upgrade APT #
apt-get upgrade -y
