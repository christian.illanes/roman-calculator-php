<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;

    final class ArabicTest extends TestCase
    {
        public function test_Arabic_convertToRoman_returnsCorrectRomanNumber()
        {
            $arabic = new Arabic();

            $testCases = (object) array(
                (object) array( "param" => 1, "expected" => "I"),
                (object) array( "param" => 2, "expected" => "II"),
                (object) array( "param" => 3, "expected" => "III"),
                (object) array( "param" => 5, "expected" => "V"),
                (object) array( "param" => 8, "expected" => "VIII"),
                (object) array( "param" => 9, "expected" => "IX"),
                (object) array( "param" => 10, "expected" => "X"),
                (object) array( "param" => 14, "expected" => "XIV"),
                (object) array( "param" => 14, "expected" => "XIV"),
                (object) array( "param" => 20, "expected" => "XX"),
                (object) array( "param" => 30, "expected" => "XXX"),
                (object) array( "param" => 39, "expected" => "XXXIX"),
                (object) array( "param" => 40, "expected" => "XL"),
                (object) array( "param" => 44, "expected" => "XLIV"),
                (object) array( "param" => 48, "expected" => "XLVIII"),
                (object) array( "param" => 49, "expected" => "XLIX"),
                (object) array( "param" => 88, "expected" => "LXXXVIII"),
                (object) array( "param" => 90, "expected" => "XC"),
                (object) array( "param" => 99, "expected" => "XCIX"),
                (object) array( "param" => 100, "expected" => "C"),
                (object) array( "param" => 399, "expected" => "CCCXCIX"),
                (object) array( "param" => 400, "expected" => "CD"),
                (object) array( "param" => 499, "expected" => "CDXCIX"),
                (object) array( "param" => 500, "expected" => "D"),
                (object) array( "param" => 899, "expected" => "DCCCXCIX"),
                (object) array( "param" => 900, "expected" => "CM"),
                (object) array( "param" => 998, "expected" => "CMXCVIII"),
                (object) array( "param" => 999, "expected" => "CMXCIX"),
                (object) array( "param" => 1000, "expected" => "M"),
                (object) array( "param" => 3999, "expected" => "MMMCMXCIX"),
            );

            foreach ($testCases as $test) {
                $result = $arabic->convertToRoman($test->param);
                $this->assertEquals($test->expected, $result);
            }
        }

        public function test_Arabic_applyDivisbleBy_modifiesPassByReferenceParametersCorrectly()
        {
            $arabic = new Arabic();

            $testCases = (object) array(
                (object) array( "arabic" => 3, "divisor" => 1, "romanRepresentation" => "I", "roman" => "", "expectedArabic" => 0, "expectedRoman" => "III"),
                (object) array( "arabic" => 4, "divisor" => 4, "romanRepresentation" => "IV", "roman" => "", "expectedArabic" => 0, "expectedRoman" => "IV"),
                (object) array( "arabic" => 7, "divisor" => 5, "romanRepresentation" => "V", "roman" => "", "expectedArabic" => 2, "expectedRoman" => "V"),
                (object) array( "arabic" => 2, "divisor" => 1, "romanRepresentation" => "I", "roman" => "V", "expectedArabic" => 0, "expectedRoman" => "VII"),
            );

            foreach ($testCases as $test) {
                $arabic->applyDivisbleBy($test->arabic, $test->divisor, $test->romanRepresentation, $test->roman);
                $this->assertEquals($test->expectedArabic, $test->arabic);
                $this->assertEquals($test->expectedRoman, $test->roman);
            }
        }
    }
?>