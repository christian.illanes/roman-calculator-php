<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;

    final class CalculatorTest extends TestCase
    {
        public function test_Calculator_addRoman_returnsCorrectRomanSum()
        {
            $calculator = new Calculator();

            $testCases = (object) array(
                (object) array( "roman1" => "II", "roman2" => "II", "expected" => "IV"),
                (object) array( "roman1" => "III", "roman2" => "II", "expected" => "V"),
                (object) array( "roman1" => "III", "roman2" => "IV", "expected" => "VII"),
            );

            foreach ($testCases as $test) {
                $result = $calculator->addRoman($test->roman1, $test->roman2);
                $this->assertEquals($test->expected, $result);
            }
        }
    }
?>