<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;

    final class RomanTest extends TestCase
    {
        public function test_Roman_convertToArabic_returnsCorrectArabicNumber()
        {
            $roman = new Roman();

            $testCases = (object) array(
                (object) array( "param" => "I", "expected" => 1),
                (object) array( "param" => "III", "expected" => 3),
                (object) array( "param" => "IV", "expected" => 4),
                (object) array( "param" => "V", "expected" => 5),
                (object) array( "param" => "VIII", "expected" => 8),
                (object) array( "param" => "IX", "expected" => 9),
                (object) array( "param" => "X", "expected" => 10),
                (object) array( "param" => "XXXVIII", "expected" => 38),
                (object) array( "param" => "XXXIX", "expected" => 39),
                (object) array( "param" => "XL", "expected" => 40),
                (object) array( "param" => "XLVIII", "expected" => 48),
                (object) array( "param" => "XLIX", "expected" => 49),
                (object) array( "param" => "L", "expected" => 50),
                (object) array( "param" => "LV", "expected" => 55),
                (object) array( "param" => "LXXXVIII", "expected" => 88),
                (object) array( "param" => "XC", "expected" => 90),
                (object) array( "param" => "XCIX", "expected" => 99),
                (object) array( "param" => "C", "expected" => 100),
                (object) array( "param" => "CCCXCIX", "expected" => 399),
                (object) array( "param" => "CD", "expected" => 400),
                (object) array( "param" => "CDXCIX", "expected" => 499),
                (object) array( "param" => "D", "expected" => 500),
                (object) array( "param" => "DCCCXCIX", "expected" => 899),
                (object) array( "param" => "CM", "expected" => 900),
                (object) array( "param" => "CMXCVIII", "expected" => 998),
                (object) array( "param" => "CMXCIX", "expected" => 999),
                (object) array( "param" => "M", "expected" => 1000),
                (object) array( "param" => "MMMCMXCIX", "expected" => 3999),
            );

            foreach ($testCases as $test) {
                $result = $roman->convertToArabic($test->param);
                $this->assertEquals($test->expected, $result);
            }
        }

        public function test_Roman_value_returnsCorrectArabicNumber()
        {
            $roman = new Roman();

            $testCases = (object) array(
                (object) array( "param" => "I", "expected" => 1),
                (object) array( "param" => "V", "expected" => 5),
                (object) array( "param" => "X", "expected" => 10),
                (object) array( "param" => "L", "expected" => 50),
                (object) array( "param" => "C", "expected" => 100),
                (object) array( "param" => "D", "expected" => 500),
                (object) array( "param" => "M", "expected" => 1000),
            );

            foreach ($testCases as $test) {
                $result = $roman->value($test->param);
                $this->assertEquals($test->expected, $result);
            }
        }
    }
?>