<?php
    class Arabic {
        function convertToRoman(int $arabic) {
            $roman = "";

            $this->applyDivisbleBy($arabic, 1000, "M", $roman);
            $this->applyDivisbleBy($arabic, 900, "CM", $roman);
            $this->applyDivisbleBy($arabic, 500, "D", $roman);
            $this->applyDivisbleBy($arabic, 400, "CD", $roman);
            $this->applyDivisbleBy($arabic, 100, "C", $roman);
            $this->applyDivisbleBy($arabic, 90, "XC", $roman);
            $this->applyDivisbleBy($arabic, 50, "L", $roman);
            $this->applyDivisbleBy($arabic, 40, "XL", $roman);
            $this->applyDivisbleBy($arabic, 10, "X", $roman);
            $this->applyDivisbleBy($arabic, 9, "IX", $roman);
            $this->applyDivisbleBy($arabic, 5, "V", $roman);
            $this->applyDivisbleBy($arabic, 4, "IV", $roman);
            $this->applyDivisbleBy($arabic, 1, "I", $roman);

            return $roman;
        }

        function applyDivisbleBy(int &$arabicDividend, int $arabicDivisor, string $romanRepresentation, string &$romanResult) {
            $quotient = floor($arabicDividend / $arabicDivisor);
            for ($i=0; $i < $quotient; $i++) {
                $arabicDividend -= $arabicDivisor;
                $romanResult .= $romanRepresentation;
            }
        }
    }
?>