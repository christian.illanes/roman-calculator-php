<?php
    class Roman {
        function convertToArabic(string $roman) {
            $arabic = 0;
            $numberOfChars = strlen($roman);

            for ($index=0; $index < $numberOfChars; $index++) {

                $value1 = $this->value($roman{$index});

                if ($index + 1 < $numberOfChars) {
                    $value2 = $this->value($roman{$index + 1});

                    if ($value1 >= $value2) {
                        $arabic += $value1;
                    }
                    else {
                        $arabic += ($value2 - $value1);
                        $index++;
                    }
                } else {
                    $arabic += $value1;
                    $index++;
                }
            }

            return $arabic;
        }

        function value(string $roman) {
            $arabic = -1;

            switch ($roman) {
                case 'I':
                    $arabic = 1;
                    break;
                case 'V':
                    $arabic = 5;
                    break;
                case 'X':
                    $arabic = 10;
                    break;
                case 'L':
                    $arabic = 50;
                    break;
                case 'C':
                    $arabic = 100;
                    break;
                case 'D':
                    $arabic = 500;
                    break;
                case 'M':
                    $arabic = 1000;
                    break;
                default:
                    $arabic = -1;
                    break;
            }

            return $arabic;
        }
    }
?>