<?php
    require_once __DIR__ . '/../vendor/autoload.php';

    class Calculator {
        function addRoman(string $roman1, string $roman2) {
            $roman = new Roman();
            $arabic1 = $roman->convertToArabic($roman1);
            $arabic2 = $roman->convertToArabic($roman2);
            $arabicSum = $arabic1 + $arabic2;

            $arabic = new Arabic();
            $romanSum = $arabic->convertToRoman($arabicSum);
            return $romanSum;
        }
    }
?>